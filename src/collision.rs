use amethyst::ecs::prelude::{System, ReadExpect, ReadStorage, Read, WriteStorage, Component, DenseVecStorage, Join, Entities};
use amethyst::renderer::{ScreenDimensions, MouseButton, Camera};
use amethyst::core::transform::Transform;
use amethyst::input::InputHandler;
use honeygame::{VIEW_X, VIEW_Y};
use amethyst::core::cgmath::{Vector3, MetricSpace};
use stopwatch::Stopwatch;



pub struct MouseToScreenSpace;
impl MouseToScreenSpace {
    pub fn get(mouse: (f32, f32), dimensions: (f32, f32)) -> (f32, f32) {
        (
            mouse.0 / dimensions.0 * VIEW_X,
            -mouse.1 / dimensions.1 * VIEW_Y + VIEW_Y
        )
    }
}
pub struct BoxCollider {
    pub x_dim: f32,
    pub y_dim: f32,
}
impl BoxCollider {
    pub fn new(x_dim: f32, y_dim: f32) -> BoxCollider {
        BoxCollider {
            x_dim,
            y_dim
        }
    }
}
impl Component for BoxCollider {
    type Storage = DenseVecStorage<Self>;
}

pub fn check_distance(dist_1: Vector3<f32>, dist2: Vector3<f32>) -> f32 {
    dist_1.truncate().distance2(dist2.truncate())
}

pub struct Clickable {
    toggle: bool
}
impl Component for Clickable {
    type Storage = DenseVecStorage<Self>;
}
impl Clickable {
    pub fn new() -> Clickable {
        Clickable {
            toggle: false,
        }
    }
    pub fn is_clicked(&self) -> bool {
        self.toggle
    }
    pub fn set_clicked(&mut self, clicked: bool) {
        self.toggle = clicked;
    }
}
pub fn check_clicked(mouse: (f64, f64), dimensions: (f32, f32), collider: &BoxCollider, transform: &Transform, c_t: Vector3<f32>) -> bool {
    let ray = MouseToScreenSpace::get((mouse.0 as f32, mouse.1 as f32), (dimensions.0, dimensions.1));
    let c_x = collider.x_dim;
    let c_y = collider.y_dim;
    let x = transform.translation.x - c_t.x;
    let y = transform.translation.y - c_t.y;
    ((ray.0 <= ((x - (c_x / 2.0)) + c_x)) && (ray.0 >= (x - (c_x / 2.0)))) &&
        ((ray.1 >= (y - (c_y / 2.0))) && (ray.1 <= (y + (c_y / 2.0))))
}
pub struct ClickSystem {
    pub timer: Stopwatch
}
impl ClickSystem {
    pub fn new() -> ClickSystem {
        let timer = Stopwatch::new();
        ClickSystem {
            timer
        }
    }
}
impl<'a> System<'a> for ClickSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, BoxCollider>,
        Read<'a, InputHandler<String, String>>,
        ReadExpect<'a, ScreenDimensions>,
        ReadStorage<'a, Camera>,
        //ReadStorage<'a, PlayerEntity>,
        //WriteStorage<'a, Animator>,
        Entities<'a>,
        WriteStorage<'a, Clickable>
    );
    fn run(&mut self, (mut transforms, colliders, inputs, dimensions, camera, entities, mut clicked): Self::SystemData) {
        if self.timer.elapsed_ms() == 0 {
            let mut c_t = Vector3::new(0.0, 0.0, 0.0);
            for (t, _cam) in (&transforms, &camera).join() {
                c_t = t.translation;
            }
            for (mut transform, collider, _entity, mut clickable) in (&mut transforms, &colliders, &*entities, &mut clicked).join() {
                if inputs.mouse_button_is_down(MouseButton::Left) && !clickable.is_clicked() {
                    if check_clicked(inputs.mouse_position().unwrap(), (dimensions.width(), dimensions.height()), collider, transform, c_t) {
                        clickable.set_clicked(true);
                        self.timer.start();
                    }
                }
            }
        } else if self.timer.elapsed_ms() >= 500 {
            self.timer.reset();
        }
    }
}
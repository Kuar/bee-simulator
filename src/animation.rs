use amethyst::ecs::{Component, System, WriteStorage, DenseVecStorage, Join};
use amethyst::renderer::SpriteRender;
use stopwatch::Stopwatch;
pub struct Animation;
pub struct Animator {
    pub timer: Stopwatch,
    pub anim_index: u16,
    pub anim_frame: u16,
    pub max_frames: u16,
    pub frames_per_second: u16
}
impl Animator {
    pub fn new(anim_index: u16, anim_frame: u16, max_frames: u16, frames_per_second: u16) -> Animator {
        Animator {
            timer: Stopwatch::new(),
            anim_index,
            anim_frame,
            max_frames,
            frames_per_second,
        }
    }
}
impl Component for Animator {
    type Storage = DenseVecStorage<Self>;
}
impl<'a> System<'a> for Animation {
    type SystemData = (
        WriteStorage<'a, Animator>,
        WriteStorage<'a, SpriteRender>,
    );
    fn run(&mut self, (mut animators, mut sprites): Self::SystemData) {
        for (mut anim, mut sprite) in (&mut animators, &mut sprites).join() {
            if anim.timer.elapsed_ms() < 1 { anim.timer.start() }
            else {
                if anim.timer.elapsed_ms() >= 1000 / anim.frames_per_second as i64 {
                    let potential_frame = anim.anim_frame + 1;
                    if potential_frame >= anim.max_frames {
                        anim.anim_frame = 0;
                        sprite.sprite_number = (anim.max_frames * anim.anim_index) as usize;
                    }
                    else {
                        anim.anim_frame += 1;
                        sprite.sprite_number = (anim.max_frames * anim.anim_index + anim.anim_frame) as usize;
                    }
                    anim.timer.restart();
                }
            }
        }
    }
}
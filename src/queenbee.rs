use amethyst::ecs::{Entities, WriteStorage, ReadStorage, System, SystemData, Component, DenseVecStorage, Join, Read};
use hive::{HiveFaction, Hive};
use amethyst::renderer::SpriteRender;
use amethyst::core::transform::Transform;
use collision::{BoxCollider, Clickable};
use animation::Animator;
use sprites;
use honeygame::SpriteStorage;

pub enum QueenState {
    Idle,
    Building,
}

pub struct QueenBee {
    pub state: QueenState,
    faction: HiveFaction,
    charges: u16
}
impl QueenBee {
    pub fn new(faction: HiveFaction) -> QueenBee {
        QueenBee {
            state: QueenState::Idle,
            faction,
            charges: 1
        }
    }
    pub fn can_build(&self) -> bool {
        self.charges > 0
    }
    pub fn remove_charge(&mut self) {
        self.charges -= 1;
    }
}
impl Component for QueenBee {
    type Storage = DenseVecStorage<Self>;
}

pub struct QueenBeeSystem;
impl<'a> System<'a> for QueenBeeSystem {
    type SystemData = (
        WriteStorage<'a, SpriteRender>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Hive>,
        WriteStorage<'a, Clickable>,
        WriteStorage<'a, Animator>,
        WriteStorage<'a, QueenBee>,
        WriteStorage<'a, BoxCollider>,
        Entities<'a>,
        Read<'a, SpriteStorage>
    );
    fn run(&mut self, (mut sprites, mut transforms, mut hives, mut clicks, mut animators, mut queens, mut colliders, entities, store): Self::SystemData) {
        for (mut queen, entity) in (&mut queens, &*entities).join() {
            match queen.state {
                QueenState::Idle => {},
                QueenState::Building => {
                    if queen.can_build() {
                        queen.remove_charge();
                        queen.state = QueenState::Idle;
                        let mut hive_t = Transform::default();
                        {
                            let queen_pos = transforms.get(entity).unwrap();
                            hive_t.translation = queen_pos.translation;
                        }
                        entities.build_entity()
                            .with(store.get_sprite(sprites::FRIENDLY_HIVE, 0), &mut sprites)
                            .with(hive_t, &mut transforms)
                            .with(Hive::new(HiveFaction::Friendly, 5), &mut hives)
                            .with(BoxCollider::new(32.0, 32.0), &mut colliders)
                            .with(Clickable::new(), &mut clicks)
                            .with(Animator::new(0, 0, 3, 4), &mut animators)
                            .build();
                    } else { queen.state = QueenState::Idle; }
                },
            }
        }
    }
}
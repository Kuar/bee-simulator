use amethyst::ecs::{System, Join, WriteStorage, Component, DenseVecStorage, Read, Entities, Entity};
use amethyst::core::transform::Transform;
use amethyst::core::timing::Time;
use amethyst::core::cgmath::Vector3;
use flower::Flower;
use hive::Hive;
use stopwatch::Stopwatch;
use animation::Animator;
use entity::{IsActive};
use collision::check_distance;
use std::cmp::Ordering;

pub enum EnemyState {
    Travelling,
    Searching,
    Returning,
    Harvesting,
    Dropping,
}
pub struct Enemy {
    pub state: EnemyState,
    pub timer: Stopwatch,
    pub movement: Vector3<f32>,
    pub speed: f32,
    pub target_entity: Option<Entity>,
    pub parent_hive: Option<Entity>
}
impl Enemy {
    pub fn new(parent: Entity) -> Enemy {
        Enemy {
            state: EnemyState::Travelling,
            timer: Stopwatch::new(),
            movement: Vector3::new(0.0, 0.0, 0.0),
            speed: 12.0,
            target_entity: None,
            parent_hive: Some(parent),
        }
    }
}
impl Component for Enemy {
    type Storage = DenseVecStorage<Self>;
}

pub struct EnemyAI;

impl<'a> System<'a> for EnemyAI {
    type SystemData = (
        WriteStorage<'a, Flower>,
        WriteStorage<'a, Hive>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, IsActive>,
        Entities<'a>,
        Read<'a, Time>,
        WriteStorage<'a, Animator>,
    );
    fn run(&mut self, (mut flowers, mut hives, mut enemies, mut transforms, mut is_active, entities, time, mut animators): Self::SystemData) {
        for (mut enemy, _enemy_ent) in (&mut enemies, &*entities).join() {
            if enemy.timer.elapsed_ms() >= 15000 {
                entities.delete(_enemy_ent).unwrap();
            }
            match enemy.state {
                EnemyState::Travelling => {
                    match enemy.target_entity {
                        Some(t_ent) => {
                            if is_active.contains(t_ent) {
                                {
                                    let enemy_t = transforms.get(_enemy_ent).unwrap();
                                    let t = transforms.get(t_ent).unwrap();
                                    let mut f = flowers.get_mut(t_ent).unwrap();
                                    let mut a = animators.get_mut(t_ent).unwrap();
                                    if check_distance(t.translation, enemy_t.translation) <= 250.0 {
                                        enemy.state = EnemyState::Harvesting;
                                        a.anim_index = 0;
                                        f.timer.start();
                                        is_active.remove(t_ent).unwrap();
                                    } else {
                                        if t.translation.x < enemy_t.translation.x {
                                            enemy.movement.x = -enemy.speed * time.delta_seconds();
                                        } else if t.translation.x > enemy_t.translation.x {
                                            enemy.movement.x = enemy.speed * time.delta_seconds();
                                        }
                                        if t.translation.y > enemy_t.translation.y {
                                            enemy.movement.y = enemy.speed * time.delta_seconds();
                                        } else if t.translation.y < enemy_t.translation.y {
                                            enemy.movement.y = -enemy.speed * time.delta_seconds();
                                        }
                                    }
                                }
                                let mut enemy_t = transforms.get_mut(_enemy_ent).unwrap();
                                enemy_t.translation += enemy.movement;
                            } else {
                                enemy.target_entity = None;
                                enemy.state = EnemyState::Searching;
                            }
                        },
                        None => { enemy.state = EnemyState::Searching; },
                    }
                },
                EnemyState::Returning => {
                    match enemy.parent_hive {
                        Some(hive_entity) => {
                            {       //New scope to avoid mutability problems
                                let enemy_t = transforms.get(_enemy_ent).unwrap();
                                let hive_t = transforms.get(hive_entity).unwrap();
                                let mut hive = hives.get_mut(hive_entity).unwrap();
                                if check_distance(enemy_t.translation, hive_t.translation) > 400.0 {
                                    if hive_t.translation.x < enemy_t.translation.x {
                                        enemy.movement.x = -enemy.speed * time.delta_seconds();
                                    } else if hive_t.translation.x > enemy_t.translation.x {
                                        enemy.movement.x = enemy.speed * time.delta_seconds();
                                    }
                                    if hive_t.translation.y > enemy_t.translation.y {
                                        enemy.movement.y = enemy.speed * time.delta_seconds();
                                    } else if hive_t.translation.y < enemy_t.translation.y {
                                        enemy.movement.y = -enemy.speed * time.delta_seconds();
                                    }
                                } else {
                                    enemy.state = EnemyState::Dropping;
                                    hive.pollen += 1;
                                }
                            }
                            let mut enemy_t = transforms.get_mut(_enemy_ent).unwrap();
                            enemy_t.translation += enemy.movement;
                        },
                        None => {},
                    }
                },
                EnemyState::Harvesting => {
                    animators.get_mut(_enemy_ent).unwrap().anim_index = 1;
                    enemy.state = EnemyState::Returning;
                },
                EnemyState::Dropping => {
                    enemy.timer.restart();
                    animators.get_mut(_enemy_ent).unwrap().anim_index = 0;
                    enemy.state = EnemyState::Searching;
                },
                EnemyState::Searching => {
                    let enemy_t = transforms.get(_enemy_ent).unwrap();
                    let closest = (&flowers, &transforms, &*entities, &animators).join()
                        .filter(|(_, _, e, _)| is_active.contains(*e))
                        .min_by(|a, b| {
                            let distance_a = check_distance(enemy_t.translation, a.1.translation);
                            let distance_b = check_distance(enemy_t.translation, b.1.translation);
                            distance_a.partial_cmp(&distance_b).unwrap_or(Ordering::Equal)
                        }); //Get closest flower
                    enemy.target_entity = Some(closest.unwrap().2);
                    enemy.state = EnemyState::Travelling;
                },
            }
        }
    }
}


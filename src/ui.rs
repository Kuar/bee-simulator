use amethyst::ecs::{System, WriteStorage, ReadStorage, Join, Component, DenseVecStorage};
use amethyst::core::cgmath::{Point2, Vector3};
use amethyst::renderer::Camera;
use amethyst::core::transform::Transform;

pub struct UiImage {
    pub position: Point2<f32>
}
impl Component for UiImage {
    type Storage = DenseVecStorage<Self>;
}

pub struct UI;

impl<'a> System<'a> for UI {
    type SystemData = (
        WriteStorage<'a, UiImage>,
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Camera>,
    );
    fn run(&mut self, (images, mut transforms, cameras): Self::SystemData) {
        let mut cam_pos = Vector3::new(0.0, 0.0, 0.0);
        for (_cam, cam_t) in (&cameras, &transforms).join() {
            cam_pos.x = cam_t.translation.x; cam_pos.y = cam_t.translation.y;
        }
        for (image, mut image_t) in (&images, &mut transforms).join() {
            image_t.translation.x = image.position.x + cam_pos.x;
            image_t.translation.y = image.position.y + cam_pos.y;
        }
    }
}
use amethyst::ecs::{System, WriteStorage,ReadStorage, Read, Join, Entities};
use amethyst::core::transform::{Transform};
use amethyst::core::timing::Time;
use amethyst::core::cgmath::Vector3;
use entity::PlayerEntity;
use amethyst::input::InputHandler;
use amethyst::input::Button;
use amethyst::renderer::{Camera, VirtualKeyCode};
use honeygame::{VIEW_X, VIEW_Y};
use queenbee::{QueenBee, QueenState};

pub struct PlayerMovement;
impl<'a> System<'a> for PlayerMovement {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, PlayerEntity>,
        ReadStorage<'a, Camera>,
        WriteStorage<'a, QueenBee>,
        Read<'a, InputHandler<String, String>>,
        Read<'a, Time>,
        Entities<'a>
    );
    fn run(&mut self, (mut transforms, entities, camera, mut queens, inputs, time, ents): Self::SystemData) {
        let mut p_t = Vector3::new(0.0, 0.0, 0.0);
        for (mut transform, _player, _player_entity) in (&mut transforms, &entities, &*ents).join() {
            let mut queen = queens.get_mut(_player_entity).unwrap();
            if inputs.button_is_down(Button::Key(VirtualKeyCode::Down)) {
                transform.translation.y -= 30.0 * time.delta_seconds();
            } else if inputs.button_is_down(Button::Key(VirtualKeyCode::Up)) {
                transform.translation.y += 30.0 * time.delta_seconds();
            }
            if inputs.button_is_down(Button::Key(VirtualKeyCode::Left)) {
                transform.translation.x -= 30.0 * time.delta_seconds();
            } else if inputs.button_is_down(Button::Key(VirtualKeyCode::Right)) {
                transform.translation.x += 30.0 * time.delta_seconds();
            }
            if inputs.button_is_down(Button::Key(VirtualKeyCode::H)) {
                queen.state = QueenState::Building;
            }
            p_t = transform.translation;
        }
        for (mut c_t, _cam) in (&mut transforms, &camera).join() {
            c_t.translation.x = (p_t.x - VIEW_X / 2.0).round();
            c_t.translation.y = (p_t.y - VIEW_Y / 2.0).round();
        }
    }
}
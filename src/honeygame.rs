use amethyst::core::cgmath::{Vector3, Point2};
use amethyst::core::transform::Transform;
use amethyst::prelude::*;
use amethyst::renderer::{
    Camera, Projection, SpriteRender, SpriteSheetHandle,
};
use sprite::{create_sprite, load_sprite_sheet, fill_rect};
use entity::{NonPlayerEntity, PlayerEntity, TerrainEntity, IsActive};
use ui::UiImage;
use collision::{BoxCollider, Clickable};
use animation::Animator;
use hive::{HiveCells, Hive, HiveFaction};
use flower::Flower;
use rand::prelude::*;
use sprites;
use enemy::{Enemy};
use queenbee::QueenBee;
use std::collections::HashMap;

pub const VIEW_X: f32 = 300.0;
pub const VIEW_Y: f32 = 300.0;

#[derive(Default)]
pub struct SpriteStorage {
    handles: HashMap<String, SpriteSheetHandle>
}
impl SpriteStorage {
    pub fn new() -> SpriteStorage {
        SpriteStorage {
            handles: HashMap::new(),
        }
    }
    pub fn add(&mut self, world: &mut World, (name, index): (&str, u64)) {
        let handle = load_sprite_sheet(world, &name, index);
        self.handles.insert(name.to_string(), handle);
    }
    pub fn get_spritesheet(&self, sprite: (&str, u64)) -> SpriteSheetHandle {
        self.handles[sprite.0].clone()
    }
    pub fn get_sprite(&self, sprite: (&str, u64), index: u16) -> SpriteRender {
        create_sprite(self.handles[sprite.0].clone(), index)
    }
}

pub struct HoneyGame;
impl <'a, 'b>SimpleState<'a, 'b> for HoneyGame {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        let mut store = SpriteStorage::new();

        world.register::<PlayerEntity>();
        world.register::<NonPlayerEntity>();
        world.register::<TerrainEntity>();
        world.register::<BoxCollider>();
        world.register::<Animator>();
        world.register::<UiImage>();
        world.register::<IsActive>();
        world.register::<HiveCells>();
        world.register::<Hive>();
        world.register::<Clickable>();
        world.register::<Flower>();
        world.register::<Enemy>();
        world.register::<QueenBee>();

        //The order sprite sheets are created is the drawing order!
        store.add(world, sprites::GROUND);
        store.add(world, sprites::FLOWER);
        store.add(world, sprites::ENEMY_HIVE);
        store.add(world, sprites::ENEMY_BEE);
        store.add(world, sprites::FRIENDLY_HIVE);
        store.add(world, sprites::FRIENDLY_BEE);
        store.add(world, sprites::HONEYBEE);
        store.add(world, sprites::QUEEN);
        store.add(world, sprites::POLLEN_COUNTER);

        let starting_position = Vector3::new(500.0, -180.0, 0.05);
        let _grid =
            fill_rect(world,
                      store.get_spritesheet(sprites::GROUND),
                      1,
                      Point2 {x: 50, y: 50},
                      Point2 {x: 0.0, y: 250.0});


        for _x in 0..600 {
            let flower_s = store.get_sprite(sprites::FLOWER, 2);
            let mut grass_t = Transform::default();
            grass_t.translation = Vector3::new(thread_rng().gen_range(0.0, 800.0), thread_rng().gen_range(-550.0, 250.0), 0.05);
            world.create_entity()
                .with(flower_s)
                .with(grass_t)
                .with(Flower::new())
                .with(IsActive)
                .with(Animator::new(1, 0, 2, 1))
                .with(BoxCollider::new(16.0, 16.0))
                .with(Clickable::new())
                .build();
        }

        let mut player_t = Transform::default();
        player_t.translation = starting_position;
        let _player = world.create_entity()
            .with(player_t)
            .with(PlayerEntity)
            .with(QueenBee::new(HiveFaction::Friendly))
            .with(Animator::new(0, 0, 2, 8))
            .with(BoxCollider::new(16.0, 16.0))
            .with(store.get_sprite(sprites::QUEEN, 0))
            .build();

        let mut hive_t = Transform::default();
        hive_t.translation = starting_position;
        let _hive = world.create_entity()
            .with(store.get_sprite(sprites::FRIENDLY_HIVE, 0))
            .with(hive_t)
            .with(Hive::new(HiveFaction::Friendly, 5))
            .with(BoxCollider::new(32.0, 32.0))
            .with(Clickable::new())
            .with(Animator::new(0, 0, 3, 4))
            .build();

        let mut enemy_hive_t = Transform::default();
        enemy_hive_t.translation = Vector3::new(300.0, 50.0, 0.05);
        let _e_hive = world.create_entity()
            .with(NonPlayerEntity)
            .with(Hive::new(HiveFaction::Enemy, 5))
            .with(enemy_hive_t)
            .with(Animator::new(0, 0, 3, 4))
            .with(store.get_sprite(sprites::ENEMY_HIVE, 0))
            .build();

        let mut ui_t = Transform::default();
        ui_t.translation.z = 0.15;
        let _ui = world.create_entity()
            .with(ui_t)
            .with(HiveCells::new(0, 6))
            .with(UiImage {position: Point2::new(150.0, 20.0)})
            .with(store.get_sprite(sprites::POLLEN_COUNTER, 0))
            .build();


        world.add_resource(store);
        initialise_camera(world);
    }
}


fn initialise_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.translation.z = 20.0;
    world
        .create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            VIEW_X,
            VIEW_Y,
            0.0,
        )))
        .with(transform)
        .build();
}

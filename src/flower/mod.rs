use amethyst::ecs::{System, WriteStorage, ReadStorage, Join, Entities, Component, DenseVecStorage};
use collision::{Clickable, check_distance};
use hive::HiveCells;
use stopwatch::Stopwatch;
use animation::Animator;
use entity::{PlayerEntity, IsActive};
use amethyst::core::transform::Transform;

pub struct Flower {
    pub timer: Stopwatch,
}
impl Flower {
    pub fn new() -> Flower {
        Flower {
            timer: Stopwatch::new()
        }
    }
}
impl Component for Flower {
    type Storage = DenseVecStorage<Self>;
}

pub struct PollenSystem;
impl<'a> System<'a> for PollenSystem {
    type SystemData = (
        WriteStorage<'a, Clickable>,
        WriteStorage<'a, Animator>,
        WriteStorage<'a, Flower>,
        WriteStorage<'a, HiveCells>,
        ReadStorage<'a, PlayerEntity>,
        ReadStorage<'a, Transform>,
        WriteStorage<'a, IsActive>,
        Entities<'a>
    );
    fn run(&mut self, (mut clicked, mut animators, mut flowers, mut hives, players, transforms, mut is_active, entities): Self::SystemData) {
        for (mut click, mut flower, entity, mut animator, transform) in (&mut clicked, &mut flowers, &*entities, &mut animators, &transforms).join() {
            if flower.timer.elapsed_ms() >= 20000 {
                flower.timer.reset();
                animator.anim_index = 1;
                click.set_clicked(false);
                is_active.insert(entity, IsActive).unwrap();
            }
            if click.is_clicked() && flower.timer.elapsed_ms() == 0 && is_active.contains(entity) {
                for (_player, player_t) in (&players, &transforms).join() {
                    if check_distance(player_t.translation, transform.translation) <= 300.0 {
                        for mut hive in (&mut hives).join() {
                            if (hive.honey_cells + 1) < hive.max_cells {
                                hive.honey_cells += 1;
                                animator.anim_index = 0;
                                flower.timer.start();
                                click.set_clicked(false);
                                is_active.remove(entity);
                            }
                        }
                    } else {
                        click.set_clicked(false);
                    }
                }
            }
        }
    }
}
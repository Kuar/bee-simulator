use amethyst::ecs::{System, WriteStorage, Read, ReadStorage, Component, Join, DenseVecStorage, Entities};
use amethyst::renderer::SpriteRender;
use entity::PlayerEntity;
use animation::Animator;
use enemy::{Enemy};
use honeygame::SpriteStorage;
use amethyst::core::transform::Transform;
use sprites;
use collision::{Clickable, check_distance};
use amethyst::utils::fps_counter::FPSCounter;

pub enum HiveFaction {
    Friendly,
    Enemy,
}

pub struct Hive {
    pub pollen: u16,
    faction: HiveFaction,
}
impl Hive {
    pub fn new(faction: HiveFaction, pollen: u16) -> Hive {
        Hive {
            pollen,
            faction
        }
    }
    pub fn faction(&self) -> &HiveFaction {
        &self.faction
    }
}
impl Component for Hive {
    type Storage = DenseVecStorage<Self>;
}

pub struct HiveCells {
    pub honey_cells: u16,
    pub max_cells: u16,
}
impl Component for HiveCells {
    type Storage = DenseVecStorage<Self>;
}
impl HiveCells {
    pub fn new(honey_cells: u16, max_cells: u16) -> HiveCells {
        HiveCells {
            honey_cells,
            max_cells
        }
    }
}

pub struct PollenCounterSystem;
impl<'a> System<'a> for PollenCounterSystem {
    type SystemData = (
        ReadStorage<'a, HiveCells>,
        WriteStorage<'a, SpriteRender>,
        ReadStorage<'a, PlayerEntity>,
        ReadStorage<'a, Animator>,
        Read<'a, FPSCounter>
    );
    fn run(&mut self, (hive, mut sprites, _players, animators, _fps): Self::SystemData) {
        for (hive, mut sprite) in (&hive, &mut sprites).join() {
            sprite.sprite_number = hive.honey_cells as usize;
//            for (mut animator, _player) in (&mut animators, &players).join() {
//                if hive.honey_cells > 0 {
//                    animator.anim_index = 1;
//                } else {
//                    animator.anim_index = 0;
//                }
//            }
        }
       // println!("FPS: {}, Dynamic entities: {}", _fps.sampled_fps(), &animators.join().count());
    }
}

pub struct HiveSystem;
impl<'a> System<'a> for HiveSystem {
    type SystemData = (
        ReadStorage<'a, PlayerEntity>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, HiveCells>,
        WriteStorage<'a, Clickable>,
        WriteStorage<'a, Hive>,

        WriteStorage<'a, Animator>,
        WriteStorage<'a, Enemy>,
        WriteStorage<'a, SpriteRender>,
        Entities<'a>,
        Read<'a, SpriteStorage>,
    );
    fn run(&mut self, (players, mut transforms, mut pollencells, mut clicks, mut hives, mut animator, mut enemies, mut sprite_render, entities, storage): Self::SystemData) {
        for (_player, player_t) in (&players, &transforms).join() {
            for (mut hive, hive_t, mut click) in (&mut hives, &transforms, &mut clicks).join() {
                if check_distance(player_t.translation, hive_t.translation) <= 750.0 && click.is_clicked() {
                    for (mut cells, _cell_t) in (&mut pollencells, &transforms).join() {
                        hive.pollen += cells.honey_cells;
                        cells.honey_cells = 0;
                        click.set_clicked(false);
                    }
                } else {
                    click.set_clicked(false);
                }
            }
        }
        for (mut hive, _entity) in (&mut hives, &*entities).join() {
            if hive.pollen >= 5 {
                let mut new_bee_t = Transform::default();
                hive.pollen -= 5;
                {
                    let hive_t = transforms.get(_entity).unwrap();
                    new_bee_t.translation = hive_t.translation;
                }
                let new_bee = entities.build_entity();
                let mut sprite = sprites::HONEYBEE;
                match hive.faction() {
                    HiveFaction::Friendly => { /* Already set to friendly bee!*/ },
                    HiveFaction::Enemy => {
                        sprite = sprites::HONEYBEE;
                    },
                }
                new_bee
                    .with(new_bee_t, &mut transforms)
                    .with(Animator::new(0, 0, 4, 20), &mut animator)
                    .with(Enemy::new( _entity), &mut enemies)
                    .with(storage.get_sprite(sprite, 0), &mut sprite_render)
                    .build();
            }
        }
    }
}
extern crate amethyst;
extern crate stopwatch;
extern crate rand;

use amethyst::prelude::*;
use amethyst::renderer::{DisplayConfig, DrawSprite, Pipeline,
                         RenderBundle, Stage, ALPHA, ColorMask};
use amethyst::core::transform::TransformBundle;
use amethyst::utils::fps_counter::FPSCounterSystem;
use amethyst::input::InputBundle;

mod honeygame;
mod sprite;
mod collision;
mod entity;
mod playermovement;
mod hive;
mod queenbee;
mod ui;
mod flower;
mod enemy;
mod animation;
use playermovement::PlayerMovement;
use honeygame::HoneyGame;
mod sprites;
use collision::ClickSystem;
use ui::UI;
use animation::Animation;
use hive::{PollenCounterSystem, HiveSystem};
use flower::PollenSystem;
use enemy::EnemyAI;
use queenbee::QueenBeeSystem;

fn main() -> amethyst::Result<()> {
    //amethyst::start_logger(Default::default());
    let path = "./Resources/display_config.ron";
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawSprite::new()
                .with_transparency(ColorMask::all(), ALPHA, None)
            ),
    );
    let game_data = GameDataBuilder::default()
        .with_bundle(RenderBundle::new(pipe, Some(config)).with_sprite_sheet_processor())?
        .with_bundle(TransformBundle::new())?
        .with_bundle(InputBundle::<String, String>::new())?
        .with(PlayerMovement, "pmove_system", &[])
        .with(ClickSystem::new(), "clickable", &[])
        .with(UI, "ui", &[])
        .with(PollenCounterSystem, "pollencountersystem", &[])
        .with(HiveSystem, "hivesystem", &[])
        .with(PollenSystem, "pollensystem", &[])
        .with(Animation, "animation", &[])
        .with(EnemyAI, "enemyai", &[])
        .with(QueenBeeSystem, "queensystem", &[])
        .with(FPSCounterSystem, "fps", &[]);

    let mut game = Application::new("./", HoneyGame, game_data)?;
    game.run();
    Ok(())
}

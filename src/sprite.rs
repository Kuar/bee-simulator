use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::cgmath::Vector3;
use amethyst::core::transform::Transform;
use amethyst::prelude::*;
use amethyst::renderer::{
    MaterialTextureSet, PngFormat, SpriteRender, SpriteSheet,
    SpriteSheetFormat, SpriteSheetHandle, Texture, TextureMetadata,
};
use amethyst::core::cgmath::Point2;

pub fn create_sprite(sprite_sheet: SpriteSheetHandle, sprite_index: u16) -> SpriteRender {
    SpriteRender {
        sprite_sheet: sprite_sheet.clone(),
        sprite_number: sprite_index as usize,
        flip_vertical: false,
        flip_horizontal: false,
    }
}
pub fn fill_rect(world: &mut World, sprite_sheet: SpriteSheetHandle, sprite_index: u16, dimensions: Point2<u16>, position: Point2<f32>) {
    for x in 0..dimensions.x {
        for y in 0..dimensions.y {
            let mut trans = Transform::default();
            trans.translation = Vector3::new(position.x + (16.0 * x as f32), position.y - (16.0 * y as f32), -10.0);
            let s = create_sprite( sprite_sheet.clone(), sprite_index);
            let _en = world.create_entity()
                .with(s)
                .with(trans)
                .build();
        }
    }
}
pub fn load_sprite_sheet(world: &mut World, sheet: &str, texture_id: u64) -> SpriteSheetHandle {
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            format!("Resources/Sprites/{}.png", sheet),
            PngFormat,
            TextureMetadata::srgb_scale(),
            (),
            &texture_storage,
        )
    };
    // `texture_id` is a application defined ID given to the texture to store in
// the `World`. This is needed to link the texture to the sprite_sheet.
    let mut material_texture_set = world.write_resource::<MaterialTextureSet>();
    material_texture_set.insert(texture_id, texture_handle);
    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        format!("Resources/SpriteRons/{}.ron", sheet), // Here we load the associated ron file
        SpriteSheetFormat,
        texture_id, // We pass it the ID of the texture we want it to use
        (),
        &sprite_sheet_store,
    )
}
use amethyst::ecs::prelude::{Component, NullStorage};
#[derive(Default)]
pub struct NonPlayerEntity;
impl Component for NonPlayerEntity {
    type Storage = NullStorage<Self>;
}
#[derive(Default)]
pub struct TerrainEntity;
impl Component for TerrainEntity {
    type Storage = NullStorage<Self>;
}
#[derive(Default)]
pub struct PlayerEntity;
impl Component for PlayerEntity {
    type Storage = NullStorage<Self>;
}#[derive(Default)]
pub struct PlayerHive;
impl Component for PlayerHive {
    type Storage = NullStorage<Self>;
}
#[derive(Default)]
pub struct IsActive;
impl Component for IsActive {
    type Storage = NullStorage<Self>;
}